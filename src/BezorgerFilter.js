import React from "react";
import "./BezorgerFilter.css";

class BezorgerFilter extends React.Component {
  onSearch = (event) => {
    if (event.target.value == "Alle") {
      let time = "";
      this.props.onSubmit(time);
    }
    else if (event.target.value == "Vandaag") {
      let time = new Date().toISOString();
      time = time.slice(0,10);
      this.props.onSubmit(time);
    }
    else if (event.target.value == "Afgelopen week") {      
      let time = new Date();
      time.setDate(time.getDate() - 7);
      time = time.toISOString();
      time = time.slice(0,10);
      this.props.onSubmit(time);
    } 
    else if (event.target.value == "Afgelopen maand") {
      let time = new Date();
      time.setMonth(time.getMonth() - 1);
      time = time.toISOString();
      time = time.slice(0,10);
      this.props.onSubmit(time);
    }
  }

  render(){
    return (
    <form className="filter">
        <label className="filter__label" htmlFor="filter">Filter op:</label>
        <select onChange={this.onSearch} className="filter__select" name="" id="">
          <option value="Alle" default>Alle</option>
          <option value="Vandaag">Vandaag</option>
          <option value="Afgelopen week">Afgelopen week</option>
          <option value="Afgelopen maand">Afgelopen maand</option>
        </select>
    </form>
    );
  }
}

export default BezorgerFilter;