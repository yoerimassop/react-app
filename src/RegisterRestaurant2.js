import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import axios from 'axios';
import LandingspageHeader from './LandingspageHeader';
// import {registerRestaurant} from './UserFunctions.js';
import { getProfile } from './UserFunctions';
import './RegisterRestaurant.css';

import RegisterRestaurant from './RegisterRestaurant';




class RegisterRestaurant2 extends React.Component{
    constructor(){
        super();
        this.state = {
            naam: '',
            priveEmail:'',
            logo: '',
            telefoonnummer: '',
            KVK_nummer: '',
            BTW_nummer: '',
            prive_nummer:'06-1234567',
            postcode:'',
            huisnummer:'',
            woonplaats:'',
            straatnaam: '',
            beschrijving: '',
            categorie:'Italiaans',
            kleur: 'white'
            
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.previewFile = this.previewFile.bind(this);
    }

    onChange(e){
    
    this.setState({[e.target.name]: e.target.value})    
    
}

registerRestaurant = newRestaurant => {
    return axios
    .post(process.env.REACT_APP_API_URL +'api/restaurant', newRestaurant, {
        headers:{'Content-Type': 'application/json'}
    })
    .then(res => {
        if(res){
            this.props.history.push('/restaurantdashboard');
        }
        
    })
    .catch(err => {
        this.setState({
            kleur: 'red'
        })
        
    })
}





doRequest(){
    console.log("usertoken:" +localStorage.usertoken);
    var usertoken = localStorage.usertoken;
    return axios
        .get(process.env.REACT_APP_API_URL + 'api/profile', {
            
            headers: { 
                Authorization: `Bearer ${usertoken}` }
        })
        .then(response => {
            return response.data
        })
        .catch(err => {
            console.log("get profile error:" + err)
        })
}



componentDidMount() {
    setTimeout(function() { 
        this.doRequest().then(res => {
            this.setState({
                priveEmail: res.user.email
            })    
        });
    }.bind(this), 2000);

    

}



    



onSubmit(e){
    e.preventDefault();

    const newRestaurant = {
        naam: this.state.naam,
        priveEmail: this.state.priveEmail,
        logo: this.state.logo,
        telefoonnummer: this.state.telefoonnummer,
        KVK_nummer: this.state.KVK_nummer,
        BTW_nummer: this.state.BTW_nummer,
        prive_nummer: this.state.prive_nummer,
        postcode: this.state.postcode,
        huisnummer: this.state.huisnummer,
        woonplaats: this.state.woonplaats,
        straatnaam: this.state.straatnaam,
        beschrijving: this.state.beschrijving,
        categorie: this.state.categorie    

    }

    console.log(newRestaurant);
    

    
    this.registerRestaurant(newRestaurant)
    // .then(     
    //     this.props.history.push('/restaurantdashboard')
    // );

    
    
}

previewFile() {
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();
  
    // reader.addEventListener("load", function () {
    // }, false);
  
    if (file) {
      reader.readAsDataURL(file);

      reader.onload = () => {
        console.log(reader.result);
        var base64 = reader.result;
        var base64_string = base64.toString();
        this.setState({
            logo: base64_string
        })
      }   
    }
  }




render(){
    console.log(this.state.priveEmail);
    return(
    <section>
        <LandingspageHeader/>
        <section className="register">
        <section className="register_back">
                <Link to="" className="register_back_arrow">&#8592;</Link>
            </section>
            <form className="register_form"  onSubmit={this.onSubmit}>
                <section className="register_form_header">
                    <hr className="register_form_header_line"/>
                    <section>
                         <h1 className="register_form_header_h1">Registreer restaurant</h1>
                         <p className="register_form_header_p">Stap 2: restaurantgegevens</p>
                    </section>
                    <hr className="register_form_header_line"/>
                </section>

            <section className="gridsection">

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="naam">Restaurant naam</label><br/>
                <input  className="register_form_input" type="text"
                 name="naam"
                 placeholder="Typ hier de naam van het restaurant" 
                 value={this.state.naam}  
                 onChange={this.onChange}
                 required
                />
                <small className="register_form_small__error" style={{ color: this.state.kleur}}>Restaurant al ingeschreven</small>
            </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="logo">Restaurant logo</label>
                <input className="register_form_input" 
                id="js--input2"
                 name="logo"
                 type="file"
                 placeholder="Logo restaurant"    
                //  value={this.state.logo}
                 onChange={this.previewFile}            
                 required
                />
            </section>

             <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="telefoonnummer">Restaurant telefoonnummer</label>
                <input type="phone"
                 className="register_form_input"
                 name="telefoonnummer"
                 minLength="6"
                 placeholder="Typ hier het telefoonnummer" 
                 value={this.state.telefoonnummer}  
                 onChange={this.onChange}
                 required
                />
             </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="KVK_nummer">KVK nummer</label>
                <small className="register_form_small">Format: 12345678</small>
                <input type="phone"
                 className="register_form_input"
                 name="KVK_nummer"
                 pattern="[0-9]{8}"
                 placeholder="Typ hier het kvk nummer" 
                 value={this.state.KVK_nummer}  
                 onChange={this.onChange}
                 required
                />
            </section>

    

           <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="BTW_nummer">BTW nummer</label>
                <small className="register_form_small">Format: NL123456789B12</small>
                <input type="text"
                 className="register_form_input"
                 name="BTW_nummer"
                 placeholder="Typ hier het btwnummer" 
                 pattern="NL[0-9]{9}B[0-9]{2}"
                 value={this.state.BTW_nummer}  
                 onChange={this.onChange}
                 required
                />
            </section>


            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="postcode">Postcode</label>
                <input type="text"
                 className="register_form_input"
                 name="postcode"
                 placeholder="Typ hier de postcode" 
                 value={this.state.postcode}  
                 onChange={this.onChange}
                 required
                 maxLength="6"
                />
            </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="huisnummer">Huisnummer</label>
                <input type="text"
                 className="register_form_input"
                 name="huisnummer"
                 placeholder="Typ hier het huisnummer" 
                 value={this.state.huisnummer}  
                 onChange={this.onChange}
                 required
                 maxLength="4"
                />
         </section>

             <section>
                <p className="register_form_star">*</p>            
                <label className="register_form_label" htmlFor="woonplaats">Woonplaats</label>
                <input type="text"
                 className="register_form_input"
                 name="woonplaats"
                 placeholder="Typ hier de woonplaats in" 
                 value={this.state.woonplaats}  
                 onChange={this.onChange}
                 required
                />
             </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="straatnaam">Straatnaam</label>
                <input type="text"
                 className="register_form_input"
                 name="straatnaam"
                 placeholder="Typ hier de straatnaam in" 
                 value={this.state.straatnaam}  
                 onChange={this.onChange}
                 required
                />
         </section>

             <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="beschrijving">Beschrijving restaurant</label>
                <textarea type="text"
                 className="register_form_input register_form_beschrijving"
                 name="beschrijving"
                 placeholder="Typ hier de beschrijving van het restaurant" 
                 value={this.state.beschrijving}  
                 onChange={this.onChange}
                 required 
                 maxLength="74"
                ></textarea>
             </section>

              <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="categorie">Eet categorie</label>
                <br/>
                
                <select 
                className="register_form_select"
                name="categorie" 
                id="Categorie"
                value={this.state.categorie}
                onChange={this.onChange}
                >
                    <option value="Italiaans">Italiaans</option>
                    <option value="Döner">Döner</option>
                    <option value="Sandwiches">Sandwiches</option>
                    <option value="Hamburger">Hamburger</option>
                    <option value="Sushi">Sushi</option>
                </select>
             </section>
            <button className="register_form_submit" type="submit">Registreer</button>
            </section>
        </form>
            <aside className="register_aside">
                <Link to="registerbezorger" className="register_aside_bezorger">Registreer als bezorger</Link>
                <Link to="registerklant" className="register_aside_restaurant">Registreer als Klant</Link>
                <hr className="register_aside_line"/>
            </aside>
        </section>
    </section>
    );
}

}



export default RegisterRestaurant2;
