import React from 'react';
import './GerechtenKlantMenu.css';


class GerechtenKlantMenuCategorie extends React.Component{

  categorieGekozen = () =>{
    this.props.categorieGekozen(this.props.categorie);
  }


  render(){
    return(
      <section>
      <li onClick={this.categorieGekozen} className="CategorieList_item">
        <p className="CategorieList_item_content">{this.props.categorie}</p>
      </li >
      </section>
    );
  }
}

export default GerechtenKlantMenuCategorie
