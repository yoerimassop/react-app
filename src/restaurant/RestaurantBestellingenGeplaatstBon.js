import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantProductNaam from './RestaurantProductNaam.js';

import "./RestaurantBestellingenGeplaatstBon.css";

class RestaurantBestellingenGeplaatstBon extends React.Component{
  constructor() {
    super();
    this.state = {
      producten: []
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer+'/producten')
    .then(response => {
      this.setState({
        producten: response.data
      })
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    return (
      <ul id="bestBon">
        {this.state.producten.map(product =>
          <li key={product.id} className="bestBon__li">
            <RestaurantProductNaam productId={product.product_id}/>
            <p className="bestBon__li__aantal">x{product.aantal}</p>
            <hr/>
          </li>
        )}
      </ul>
    )
  }
}

export default RestaurantBestellingenGeplaatstBon;
