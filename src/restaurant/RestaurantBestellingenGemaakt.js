import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGemaaktCard from './RestaurantBestellingenGemaaktCard.js';
import {getProfile} from '../UserFunctions';

import "./RestaurantBestellingenGemaakt.css";

class RestaurantBestellingenGemaakt extends React.Component{
  constructor() {
    super();
    this.state = {
      bestellingen: []
    }
    this.child1Ref = React.createRef();
  }

  componentDidMount() {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  collapseList = () => {
    document.getElementById("gemaBest__ul").style.height = "0";
    document.getElementById("gemaBest__titel__collapse").style.display = "none";
    document.getElementById("gemaBest__titel__expand").style.display = "inline-block";
  }

  expandList = () => {
    document.getElementById("gemaBest__ul").style.height = "45rem";
    document.getElementById("gemaBest__titel__collapse").style.display = "inline-block";
    document.getElementById("gemaBest__titel__expand").style.display = "none";
  }

  updateBestellingen = () => {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
            this.child1Ref.current.updateStatus();
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  render(){
    return (
      <section id="gemaBest__section">
        <section id="gemaBest">
          <section id="gemaBest__titel">
            <h3>In afwachting / onderweg</h3>
            <section id="gemaBest__titel__buttons">
              <button onClick={this.collapseList} id="gemaBest__titel__collapse"><i className="arrow up" /></button>
              <button onClick={this.expandList} id="gemaBest__titel__expand"><i className="arrow down" /></button>
            </section>
          </section>
          <ul id="gemaBest__ul">
            {this.state.bestellingen.reverse().filter(bestelling => bestelling.status === "gemaakt" || bestelling.status === "geclaimd").map(bestelling =>
              <RestaurantBestellingenGemaaktCard ref={this.child1Ref} key={bestelling.ordernummer} orderNummer={bestelling.ordernummer}/>
            )}
          </ul>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingenGemaakt;
