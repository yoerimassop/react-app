import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';

import "./RestaurantBezorgerNaam.css";

class RestaurantBezorgerNaam extends React.Component{
  constructor() {
    super();
    this.state = {
      bezorger: []
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'user/'+this.props.emailBezorger)
    .then(response => {
      this.setState({
        bezorger: response.data
      })
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    return (
      <p className="card__naamBezorger">Bezorger: {this.state.bezorger.naam}</p>
    )
  }
}

export default RestaurantBezorgerNaam;
