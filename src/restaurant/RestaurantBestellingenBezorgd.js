import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenBezorgdCard from './RestaurantBestellingenBezorgdCard.js';
import {getProfile} from '../UserFunctions';

import "./RestaurantBestellingenBezorgd.css";

class RestaurantBestellingenBezorgd extends React.Component{
  constructor() {
    super();
    this.state = {
      bestellingen: []
    }
  }

  componentDidMount() {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }
  collapseList = () => {
    document.getElementById("bezBest__ul").style.height = "0";
    document.getElementById("bezBest__titel__collapse").style.display = "none";
    document.getElementById("bezBest__titel__expand").style.display = "inline-block";
  }

  expandList = () => {
    document.getElementById("bezBest__ul").style.height = "45rem";
    document.getElementById("bezBest__titel__collapse").style.display = "inline-block";
    document.getElementById("bezBest__titel__expand").style.display = "none";
  }

  updateBestellingen = () => {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          axios
          .get(process.env.REACT_APP_API_URL + 'bestelling/restaurant/'+ response.data.naam)
          .then(response => {
            this.setState({
              bestellingen: response.data
            })
          })
          .catch(error => {
            console.log(error);
          })
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  render(){
    return (
      <section id="bezBest__section">
        <section id="bezBest">
          <section id="bezBest__titel">
            <h3>Bezorgde bestellingen</h3>
            <section id="bezBest__titel__buttons">
              <button onClick={this.collapseList} id="bezBest__titel__collapse"><i className="arrow up" /></button>
              <button onClick={this.expandList} id="bezBest__titel__expand"><i className="arrow down" /></button>
            </section>
          </section>
          <ul id="bezBest__ul">
            {this.state.bestellingen.reverse().filter(bestelling => bestelling.status === "bezorgd").map(bestelling =>
              <RestaurantBestellingenBezorgdCard key={bestelling.ordernummer} orderNummer={bestelling.ordernummer} />
            )}
          </ul>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingenBezorgd;
