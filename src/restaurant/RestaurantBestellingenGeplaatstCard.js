import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGeplaatstModal from './RestaurantBestellingenGeplaatstModal.js';
import RestaurantKlantNaam from './RestaurantKlantNaam.js';

import "./RestaurantBestellingenGeplaatstCard.css";

class RestaurantBestellingenGeplaatstCard extends React.Component{
  constructor() {
    super();
    this.state = {
      ordernummer: '',
      totaalbedrag: '',
      bezorgdatum: '',
      bezorgtijd: '',
      betaalmethode: '',
      email_klant: '',
      email_bezorger: '',
      naam_restaurant: '',
      status: '',
      bezorgadres: ''
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
    })
    .catch(error => {
      console.log(error);
    })
  }

  openModal = (e)=> {
    const modalId = "geplBest__modal"+this.state.ordernummer;
    document.getElementById(modalId).style.display = "inline-block";
  };

  updateBestellingen = () => {
    this.props.updateBestellingen();
  }

  render(){
    const modalId = "geplBest__modal"+this.state.ordernummer;

    //Voorkomt dat de child component een lege prop krijgt opgestuurd
    let restaurantBestellingenGeplaatstModal;
    if(this.state.ordernummer > 0) {
      restaurantBestellingenGeplaatstModal = <RestaurantBestellingenGeplaatstModal orderNummer={this.state.ordernummer} updateBestellingen={this.updateBestellingen} />;
    }
    else {restaurantBestellingenGeplaatstModal = null}

    let restaurantKlantNaam;
    if(this.state.ordernummer > 0) {
      restaurantKlantNaam = <RestaurantKlantNaam emailKlant={this.state.email_klant}/>;
    }
    else {restaurantKlantNaam = null}

    return (
      <li className="geplBest__card" key={this.state.ordernummer}>
        <p className="geplBest__card__ordernr">Ordernummer #{this.state.ordernummer}</p>
        <p className="geplBest__card__naamKlant">Naam: {restaurantKlantNaam}</p>
        <p className="geplBest__card__bezorgAdres">Naar: {this.state.bezorgadres}</p>
        <p className="geplBest__card__bezordTijd">Bezorgtijd: {this.state.bezorgtijd}</p>
        <section>
          <button className="geplBest__card__button" onClick={this.openModal}>Bekijk bestelling</button>
        </section>
        <section className="geplBest__modal" id={modalId} >
          {restaurantBestellingenGeplaatstModal}
        </section>
      </li>
    )
  }
}

export default RestaurantBestellingenGeplaatstCard;
