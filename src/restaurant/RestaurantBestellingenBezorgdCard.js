import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGemaaktModal from './RestaurantBestellingenGemaaktModal.js';
import RestaurantKlantNaam from './RestaurantKlantNaam.js';
import RestaurantBezorgDatum from './RestaurantBezorgDatum.js';

import "./RestaurantBestellingenBezorgdCard.css";

class RestaurantBestellingenBezorgdCard extends React.Component{
  constructor() {
    super();
    this.state = {
      ordernummer: '',
      totaalbedrag: '',
      bezorgdatum: '',
      bezorgtijd: '',
      betaalmethode: '',
      email_klant: '',
      email_bezorger: '',
      naam_restaurant: '',
      status: '',
      bezorgadres: ''
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
    })
    .catch(error => {
      console.log(error);
    })
  }

  openModal = (e)=> {
    const modalId = "gemaBest__modal"+this.state.ordernummer;
    document.getElementById(modalId).style.display = "inline-block";
  };

  render(){
    const modalId = "gemaBest__modal"+this.state.ordernummer;

    //Voorkomt dat de child component een lege prop krijgt opgestuurd
    let restaurantBestellingenGemaaktModal;
    if(this.state.ordernummer > 0) {
      restaurantBestellingenGemaaktModal = <RestaurantBestellingenGemaaktModal orderNummer={this.state.ordernummer} />;
    }
    else {restaurantBestellingenGemaaktModal = null}

    let restaurantKlantNaam;
    if(this.state.ordernummer > 0) {
      restaurantKlantNaam = <RestaurantKlantNaam emailKlant={this.state.email_klant}/>;
    }
    else {restaurantKlantNaam = null}

    let restaurantBezorgDatum;
    if(this.state.ordernummer > 0) {
      restaurantBezorgDatum = <RestaurantBezorgDatum bezorgdatum={this.state.bezorgdatum}/>;
    }
    else {restaurantBezorgDatum = null}

    return (
      <li className="bezBest__card" key={this.state.ordernummer}>
        <p className="bezBest__card__ordernr">Ordernummer #{this.state.ordernummer}</p>
        <p className="bezBest__card__naamKlant">Naam: {restaurantKlantNaam}</p>
        <p className="bezBest__card__bezorgAdres">Naar: {this.state.bezorgadres}</p>
        <p className="bezBest__card__bezordTijd"><span>Bezorgd op: </span>{restaurantBezorgDatum}</p>
        <section>
          <button className="bezBest__card__button" onClick={this.openModal}>Bekijk bestelling</button>
        </section>
        <section className="gemaBest__modal" id={modalId} >
          {restaurantBestellingenGemaaktModal}
        </section>
      </li>
    )
  }
}

export default RestaurantBestellingenBezorgdCard;
