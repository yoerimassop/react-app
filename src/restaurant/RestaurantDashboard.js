import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import DeliveryPageHeader from '.././DeliveryPageHeader';
import ReviewModal from '../ReviewModal.js';
import axios from 'axios';
import {getProfile} from '../UserFunctions';
import "./RestaurantDashboard.css";

class RestaurantDashboard extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      showReviewModal: false,
      restaurant: ''
    }
    this.closeReviewModal = this.closeReviewModal.bind(this);
    this.showReviewModal = this.showReviewModal.bind(this);
  }

  showReviewModal(){
    this.setState({showReviewModal: true});
  }

  closeReviewModal(){
    this.setState({showReviewModal: false});
  }

  reviewModal(){
    let reviewModal;
    if(this.state.showReviewModal === true){
      return <ReviewModal showButton={'false'} restaurantNaam={this.state.restaurant} closeReviewModal={this.closeReviewModal}/>
    }
    else{
      console.log("geen review modal");
    }
  }

  componentDidMount() {
    getProfile().then(res => {
      axios
        .get(process.env.REACT_APP_API_URL + 'restaurant/'+res.user.email)
        .then(response => {
          this.setState({restaurant: response.data.naam})
        })
        .catch(error => {
          console.log(error);
        })
    })
  }

  render(){
    return (
      <section>
        <section id="restDash__main">
          <DeliveryPageHeader
            titel="Welkom op het restaurantdashboard"
            ondertitel="De beheeromgeving voor jou als ondernemer."
          />
          <section id="restDash__head">
            <section>
              <hr/>
            </section>
            <h2>Ondernemer Dashboard</h2>
            <section>
              <hr/>
            </section>
          </section>
          <br/>
          <section id="restDash__body">
            <section id="restDash__gerechten" className="restDash__card">
              <p>Voeg hier nieuwe gerechten toe of bewerk gerechten</p>
              <Link to="gerechtoverzicht" className="restDash__button">Gerechten editor</Link>
            </section>
            <section id="restDash__bestellingen" className="restDash__card">
              <p>Zie en beheer hier geplaatste bestellingen bij uw restaurant</p>
              <Link to="restaurantbestellingen" className="restDash__button">Zie bestellingen</Link>
            </section>
            <section id="restDash__reviews" className="restDash__card">
              <p>Zie en reageer op geplaatste reviews over uw restaurant</p>
              <Link onClick={this.showReviewModal} className="restDash__button">Zie reviews</Link>
            </section>
          </section>
        </section>
        {this.reviewModal()}
      </section>
    )
  }
}

export default RestaurantDashboard;
