import React from 'react';
import axios from 'axios';
import Moment from 'moment';

class RestaurantBezorgDatum extends React.Component{
  constructor(){
    super();
    this.state = {
      bezorgdatumDMY: '',
    }
  }

  componentDidMount(){
    this.setState({bezorgdatumDMY: Moment(this.props.bezorgdatum).format('DD-MM-YYYY')})
  }

  render(){
    return (
      <span>{this.state.bezorgdatumDMY}</span>
    )
  }
}

export default RestaurantBezorgDatum;
