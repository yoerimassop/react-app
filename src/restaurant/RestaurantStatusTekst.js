import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';

import "./RestaurantStatusTekst.css";

class RestaurantStatusTekst extends React.Component{
  render(){
    const statusColor = {
      backgroundColor: "#E28800"
    }
    const statusProp = this.props.status;
    let status;
    if(statusProp === "gemaakt"){
      status = "In afwachting"
    }
    else{
      status = "Geclaimd";
      statusColor.backgroundColor = "#00AC1C";
    }

    return (
      <section className="card__status" style={statusColor}>
        <p className="card__status__p">{status}</p>
      </section>
    )
  }
}

export default RestaurantStatusTekst;
