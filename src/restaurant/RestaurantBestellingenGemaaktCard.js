import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGemaaktModal from './RestaurantBestellingenGemaaktModal.js';
import RestaurantKlantNaam from './RestaurantKlantNaam.js';
import RestaurantStatusTekst from './RestaurantStatusTekst.js';

import "./RestaurantBestellingenGemaaktCard.css";

class RestaurantBestellingenGemaaktCard extends React.Component{
  constructor() {
    super();
    this.state = {
      ordernummer: '',
      totaalbedrag: '',
      bezorgdatum: '',
      bezorgtijd: '',
      betaalmethode: '',
      email_klant: '',
      email_bezorger: '',
      naam_restaurant: '',
      status: '',
      bezorgadres: ''
    }
    this.child1Ref = React.createRef();
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
    })
    .catch(error => {
      console.log(error);
    })
  }

  openModal = (e)=> {
    const modalId = "gemaBest__modal"+this.state.ordernummer;
    document.getElementById(modalId).style.display = "inline-block";
  };

  updateStatus = () => {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
      console.log("status geupdate");
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    const modalId = "gemaBest__modal"+this.state.ordernummer;
    //Voorkomt dat de child component een lege prop krijgt opgestuurd
    let restaurantBestellingenGemaaktModal;
    if(this.state.ordernummer > 0) {
      restaurantBestellingenGemaaktModal = <RestaurantBestellingenGemaaktModal orderNummer={this.state.ordernummer}/>;
    }
    else {restaurantBestellingenGemaaktModal = null}

    let restaurantKlantNaam;
    if(this.state.ordernummer > 0) {
      restaurantKlantNaam = <RestaurantKlantNaam emailKlant={this.state.email_klant}/>;
    }
    else {restaurantKlantNaam = null}

    let restaurantStatusTekst;
    if(this.state.ordernummer > 0) {
      restaurantStatusTekst = <RestaurantStatusTekst ref={this.child1Ref} status={this.state.status}/>;
    }
    else {restaurantStatusTekst = null}

    return (
      <li className="gemaBest__card" key={this.state.ordernummer}>
        <p className="gemaBest__card__ordernr">Ordernummer #{this.state.ordernummer}</p>
        <p className="gemaBest__card__naamKlant">Naam: {restaurantKlantNaam}</p>
        <p className="gemaBest__card__bezorgAdres">Naar: {this.state.bezorgadres}</p>
        <p className="gemaBest__card__bezordTijd">Bezorgtijd: {this.state.bezorgtijd}</p>
        <section className="gemaBest__card__details">
          {restaurantStatusTekst}
          <button className="gemaBest__card__details__button" onClick={this.openModal}>Details</button>
        </section>
        <section className="gemaBest__modal" id={modalId} >
          {restaurantBestellingenGemaaktModal}
        </section>
      </li>
    )
  }
}

export default RestaurantBestellingenGemaaktCard;
