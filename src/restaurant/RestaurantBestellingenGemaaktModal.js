import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';
import RestaurantBestellingenGeplaatstBon from './RestaurantBestellingenGeplaatstBon.js';
import RestaurantKlantNaam from './RestaurantKlantNaam.js';
import RestaurantBezorgerNaam from './RestaurantBezorgerNaam.js';

import "./RestaurantBestellingenGemaaktModal.css";

class RestaurantBestellingenGemaaktModal extends React.Component{
  constructor() {
    super();
    this.state = {
      ordernummer: '',
      totaalbedrag: '',
      bezorgdatum: '',
      bezorgtijd: '',
      betaalmethode: '',
      email_klant: '',
      email_bezorger: '',
      naam_restaurant: '',
      status: '',
      bezorgadres: ''
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'bestelling/'+this.props.orderNummer)
    .then(response => {
      this.setState(response.data);
    })
    .catch(error => {
      console.log(error);
    })
  }

  closeModal = ()=> {
    const modalId = "gemaBest__modal"+this.state.ordernummer;
    document.getElementById(modalId).style.display = "none";
  };

  render(){
    //Voorkomt dat de child component een lege prop krijgt opgestuurd
    let restaurantBestellingenGeplaatstBon;
    if(this.state.ordernummer > 0) {
      restaurantBestellingenGeplaatstBon = <RestaurantBestellingenGeplaatstBon orderNummer={this.state.ordernummer}/>;
    }
    else {restaurantBestellingenGeplaatstBon = null}

    let restaurantKlantNaam;
    if(this.state.ordernummer > 0) {
      restaurantKlantNaam = <RestaurantKlantNaam emailKlant={this.state.email_klant}/>;
    }
    else {restaurantKlantNaam = null}

    let restaurantBezorgerNaam;
    if(this.state.ordernummer > 0 && this.state.email_bezorger != null) {
      restaurantBezorgerNaam = <RestaurantBezorgerNaam emailBezorger={this.state.email_bezorger}/>;
    }
    else {restaurantBezorgerNaam = null}

    return (
      <section id="gemaModal">
        <section id="gemaModal__titel">
          <p>Ordernummer #{this.state.ordernummer}</p>
        </section>
        <section id="gemaModal__details">
          <section id="gemaModal__details__klant">
            <p className="gemaModal__details__klant__p">{restaurantKlantNaam}</p>
            <p className="gemaModal__details__klant__p">{this.state.bezorgadres}</p>
            <p className="gemaModal__details__klant__p">Bezorgtijd: {this.state.bezorgtijd}</p>
          </section>
          <section id="gemaModal__details__bestelling">
            {restaurantBestellingenGeplaatstBon}
          </section>
        </section>
        <br/>
        {restaurantBezorgerNaam}
        <section id="gemaModal__buttons">
          <button onClick={this.closeModal} id="gemaModal__buttons__return">Terug naar overzicht</button>
        </section>
      </section>
    )
  }
}

export default RestaurantBestellingenGemaaktModal;
