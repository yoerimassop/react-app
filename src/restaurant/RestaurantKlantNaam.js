import React from 'react';
import axios from 'axios';
import {Link, withRouter} from 'react-router-dom';

import "./RestaurantKlantNaam.css";

class RestaurantKlantNaam extends React.Component{
  constructor() {
    super();
    this.state = {
      klant: []
    }
  }

  componentDidMount() {
    axios
    .get(process.env.REACT_APP_API_URL + 'user/'+this.props.emailKlant)
    .then(response => {
      this.setState({
        klant: response.data
      })
    })
    .catch(error => {
      console.log(error);
    })
  }

  render(){
    return (
      <span className="card__naamKlant">{this.state.klant.naam}</span>
    )
  }
}

export default RestaurantKlantNaam;
