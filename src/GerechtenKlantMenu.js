import React from 'react';
import './GerechtenKlantMenu.css';
import GerechtenKlantMenuCategorie from './GerechtenKlantMenuCategorie';


class GerechtenKlantMenu extends React.Component{

  categorieGekozen = (categorie) =>{
    this.props.categorieGekozen(categorie);
  }

  zieAlleGekozen = () =>{
    this.props.categorieGekozen('Zie Alle');
  }

  render(){
    return(
      <section className="GerechtenKlantMenu">
        <h1 className="CategorieList_title">Categorie<span className="CategorieList_arrow"></span></h1>
        <ul className="CategorieList">
          <li onClick={this.zieAlleGekozen} className="CategorieList_item">
            <p className="CategorieList_item_content">Zie alle</p>
          </li >
          {this.props.categorie.map((categorie)=>
          <div>
            <GerechtenKlantMenuCategorie categorieGekozen={this.categorieGekozen} categorie={categorie.categorie}/>
          </div>
          )}
        </ul>
      </section>
    );
  }
}

export default GerechtenKlantMenu
