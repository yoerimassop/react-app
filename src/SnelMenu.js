import React from 'react';
import "./SnelMenu.css";
import SnelMenuItem from './SnelMenuItem';

class SnelMenu extends React.Component{
  constructor(props){
    super(props)
    this.openSnelmenu = false;
  }

  componentDidMount(){

  }

  componentWillUnmount(){

  }

  snelMenuItemClicked = (categorie) =>{
    this.props.snelMenuItemClicked(categorie)
    if(this.openSnelmenu === true){
      this.closeSnelmenuModal()
    }
  }
  openSnelmenuModal = () =>{
    this.openSnelmenu = true;
    this.props.openCloseSnelmenuModal(document.getElementById('snelMenuButton--id'), document.getElementById('snelMenu--id'), this.openSnelmenu)
  }

  closeSnelmenuModal = () =>{
    this.openSnelmenu = false;
    this.props.openCloseSnelmenuModal(document.getElementById('snelMenuButton--id'), document.getElementById('snelMenu--id'), this.openSnelmenu)
  }

  render(){
    /*window.onscroll = function() {stickSnelmenuToTop()};
    let snelmenu = document.getElementById("SnelMenu");
    let sticky = snelmenu.getBoundingClientRect();
    function stickSnelmenuToTop() {
      if (window.pageYOffset > sticky) {
        sticky.classList.add("Snelmenu_sticky");
      } else {
        sticky.classList.remove("Snelmenu_sticky");
      }
    }*/
    return(
      <section>
      <button onClick={this.openSnelmenuModal} id='snelMenuButton--id' className="Snelmenu_titel_Mobile">Snel Menu</button>
      <ul id='snelMenu--id' className='Snelmenu'>

        <h1 className="Snelmenu_titel">Snel Menu</h1>
        <span onClick={this.closeSnelmenuModal} className="Snelmenu_close" id='snelMenuClose--id'></span>
        <SnelMenuItem title="Döner" snelMenuItemClicked={this.snelMenuItemClicked}/>
        <SnelMenuItem title="Hamburger" snelMenuItemClicked={this.snelMenuItemClicked}/>
        <SnelMenuItem title="Sushi" snelMenuItemClicked={this.snelMenuItemClicked}/>
        <SnelMenuItem title="Sandwiches" snelMenuItemClicked={this.snelMenuItemClicked}/>
        <SnelMenuItem title="Italiaans" snelMenuItemClicked={this.snelMenuItemClicked}/>
        <SnelMenuItem title="Zie Alle" snelMenuItemClicked={this.snelMenuItemClicked}/>
      </ul>
      </section>
    );
  }
}

export default SnelMenu
