import React from 'react';
import LandingspageHeader from './LandingspageHeader.js';
import Info from './info.js';
import Landingspagepicure from './Landingspagepicture';

import {BrowserRouter as Router, Route} from "react-router-dom";
import Login from './Login';
import Register from './Register';


function Main() {
    return (
        <section>
        <LandingspageHeader/>
        <Landingspagepicure/>
        <Info/> 
        </section>
    );

}


export default Main;