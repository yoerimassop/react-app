import React from 'react'
import "./BezorgerCard.css"
import {Link} from "react-router-dom";
import {getProfile} from "./UserFunctions";

class BezorgerGegevens extends React.Component{
  constructor() {
    super()
    this.state = {
        naam: '',
        email: '',
        telefoonnummer: '',
        geslacht: '',
        postcode: '',
        huisnummer: '',
        woonplaats: '',
        straatnaam: '',
    }
  }

  componentDidMount() {

    getProfile().then(res => {
        console.log(res);

        this.setState({
            naam: res.user.naam,
            email: res.user.email,
            telefoonnummer: res.user.telefoonnummer,
            geslacht: res.user.geslacht,
            postcode: res.user.postcode,
            huisnummer: res.user.huisnummer,
            woonplaats: res.user.woonplaats,
            straatnaam: res.user.straatnaam
        })
    })
  }

  render(){

    return (
      <article className="card">
        <header className="card__header">
          <h2>Jouw Gegevens</h2>
        </header>
        <section className="card__content">
          <ul className="card__content__list">
            <li className="card__content__list__item">
              <p><b>Persoonsgegevens</b></p>
              <p><b>Naam:</b> {this.state.naam}</p>
              <p><b>E-mail:</b> {this.state.email}</p>
              <p><b>Telefoonnummer:</b>  {this.state.telefoonnummer}</p>
              <p><b>Geslacht</b>  {this.state.geslacht}</p>
            </li>
            <li className="card__content__list__item">
              <p><b>Adresgegevens</b></p>
              <p><b>Postcode:</b> {this.state.postcode}</p>
              <p><b>Huisnummer:</b> {this.state.huisnummer}</p>
              <p><b>Woonplaats:</b> {this.state.woonplaats}</p>
              <p><b>Straatnaam:</b> {this.state.straatnaam}</p>
              <Link to="/edituser"><button className="card__content__list__button">Wijzig gegevens</button></Link>
            </li>
          </ul>
        </section>
      </article>
    );
  }
}

export default BezorgerGegevens;
