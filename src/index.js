import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Delivery from './Delivery';
import { Provider } from 'react-redux';
import {createStore} from 'redux';
import allReducers from './reducers';

//React states opslaan na refresh
function saveToLocalStorage(state){
  try{
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch(e){
    console.log(e) //Als browser in privacy mode zit log error
  }
}

//React states laden na refresh
function loadFromLocalStorage(){
  try{
    const serializedState = localStorage.getItem('state')
    if(serializedState === null ) return undefined
    return JSON.parse(serializedState)
  }catch(e){
    console.log(e)
    return undefined
  }
}

const persistedState = loadFromLocalStorage();
export const store = createStore(allReducers, persistedState,
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

store.subscribe(() => saveToLocalStorage(store.getState()));


ReactDOM.render(
  //Redux
  //Provider onderdeel van redux
  <Provider store={store}>
  <React.StrictMode>
    <App />
  </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);
