import React from 'react';
import "./BestelRestaurantCard.css";
import star from "./images/star.png";

class BestelRestaurantCard extends React.Component{

    restaurantCardClicked = () =>{
      console.log(this.props.naam)
      this.props.restaurantCardClicked(this.props.naam)
    }

    render(){
      return(
        <section className="BestelRestaurantCard" onClick={this.restaurantCardClicked}>
          <img className="BestelRestaurantCard_img" src={this.props.imgCategorieSrc}
          alt="restaurant plaatje"/>
          <main className="BestelRestaurantCard_main">
            <img className="BestelRestaurantCard_main_logo" src={this.props.imgLogoSrc}
            alt="logo restaurant"/>
            <h1 className="BestelRestaurantCard_main_naam">{this.props.naam || "Panini"}</h1>
            <p className="BestelRestaurantCard_main_beschrijving">{this.props.beschrijving || "Lekkerste pizzaria van leiden, Pizza's, Pasta's, Kip en veel andere lekkernei"}</p>
            <div className="BestelRestaurantCard_main_waardering_flex">
              <p className="BestelRestaurantCard_main_waardering_score">Score: {this.props.waardering || "10/10"}</p>
              <img className="BestelRestaurantCard_main_waadering_star" alt="waardeeringsster" src={star}/>
            </div>
          </main>
        </section>
      );
    }
  }

export default BestelRestaurantCard
