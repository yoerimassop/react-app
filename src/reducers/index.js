import adresReducer from "./adresKlantBestelling";
import restaurantReducer from "./restaurantListReducer"
import gekozenRestaurantReducer from "./gekozenRestaurant"
import gekozenRestaurantCategorieReducer from "./gekozenRestaurantCategorie"
import {combineReducers} from  "redux";

const allReducers = combineReducers({
  adresKlantBestelling: adresReducer,
  restaurantListRedux: restaurantReducer,
  gekozenRestaurant: gekozenRestaurantReducer,
  gekozenCategorie: gekozenRestaurantCategorieReducer
});

export default allReducers;
