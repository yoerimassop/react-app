const restaurantReducer = (state = "", action) =>{
  switch(action.type){
    case 'RESTAURANT_LIJST_BESCHIKBAAR':
      return action.payload
    default:
      return state;
  }
};

export default restaurantReducer;
