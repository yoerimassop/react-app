export const adresActie = (adres) =>{
  return{
    type: "INGEVOERD_ADRES",
    payload: adres
  }
};

export const restaurantListActie = (restaurants) =>{
  return{
    type: 'RESTAURANT_LIJST_BESCHIKBAAR',
    payload: restaurants
  }
}

export const keuzeRestaurantActie = (restaurant) =>{
  return{
    type: 'RESTAURANT_GEKOZEN',
    payload: restaurant
  }
}

export const keuzeRestaurantCategorieActie = (restaurant) =>{
  return{
    type: 'CATEGORIE_GEKOZEN',
    payload: restaurant
  }
}
