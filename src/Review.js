import React from 'react';
import "./Review.css";

class Review extends React.Component{

    render(){
        return(
          <section className="review">
            <h1>{this.props.name}</h1>
            <section className="review_stars">{this.props.stars}</section>
            <p>{this.props.review}</p>
          </section>
      );
    }
}

export default Review;
