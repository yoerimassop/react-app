import { Map, GoogleMap, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';
import React, { Component, useState } from 'react';
import Geocode from "react-geocode";
import "./BezorgerLocatieMap.css";
import axios from "axios";
import BezorgerZoekbalk from './BezorgerZoekbalk';
import {getProfile} from "./UserFunctions";

export class MapContainer extends Component {
    constructor() {
        super()
        this.state = {
          centerLat: null,
          centerLng: null,
          ownLat: null,
          ownLng: null,
          searchLat: null,
          searchLng: null,
          email_bezorger: ""
        }
        this.getLocation = this.getLocation.bind(this);
      }

  getLocation(){
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(
          (position) => {
            console.log(position.coords.latitude, position.coords.longitude);
            this.setState({
              ownLat: position.coords.latitude,
              ownLng: position.coords.longitude,
              centerLat: position.coords.latitude,
              centerLng: position.coords.longitude
          },function(){this.updateLocationInDatabase()})
          },
          (error) => {
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true}
      );
    }
  }

  updateLocationInDatabase(){
    console.log("asdffdasafsdafsdasdfasfdafdsadfsafsdfds");
    Geocode.setApiKey("AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc");
    Geocode.fromLatLng(this.state.ownLat, this.state.ownLng).then(
      response => {
        console.log(response.results[0].formatted_address);
        axios.patch(process.env.REACT_APP_API_URL + 'api/bezorger/location/update/' + this.state.email_bezorger + "/" + response.results[0].formatted_address,{
          headers:{"Content-Type": "application/json"},
          _method: 'patch'
        })
      }
    )
  }

  updateUserEmail(){
    getProfile().then(res => {
        this.setState({
            email_bezorger: res.user.email
          })
      })
  }

  setLatAndLng = (searchterm) => {
    Geocode.setApiKey("AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc");
    Geocode.fromAddress(searchterm).then(
      response => {
        const { lat, lng } = response.results[0].geometry.location;
        this.setState({searchLat: lat, searchLng: lng, centerLat: lat, centerLng: lng})
        console.log(lat, lng);
      },
      error => {
        console.error(error);
      }
    )
  }

  componentDidMount(){
    this.getLocation()
    this.updateUserEmail()
  };

  render() {
    let iconMarker = new window.google.maps.MarkerImage(
        "http://maps.google.com/mapfiles/kml/shapes/cycling.png",
        null,
        null,
        null,
        new window.google.maps.Size(32, 32)
    );
      return (
        <section className="map2">
        <BezorgerZoekbalk placeholder="Zoek op adres of restaurant..." onSubmit={this.setLatAndLng}/>
        <Map
            google={this.props.google}
            zoom={13}
            center={{ lat: this.state.centerLat, lng: this.state.centerLng}}
        >
            <Marker icon={iconMarker} position={{ lat: this.state.ownLat, lng: this.state.ownLng }} />
            <Marker position={{ lat: this.state.searchLat, lng: this.state.searchLng }} />
        </Map>
        </section>
      )
    };
}


  export default GoogleApiWrapper({
  apiKey: 'AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc'
})(MapContainer);
