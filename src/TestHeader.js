import React from 'react';
import "./BestelHeader.css";
import logo from './images/logo.png';
import { Link } from "react-router-dom";
import SideBar from './SideBar';
import PlacesAutocomplete from 'react-places-autocomplete';
import login from './images/login.png';





class TestHeader extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      width: '',
      address: ''
    }
  }


handleChange = address => {
    this.setState({
        address
    });


}
  openNav = ()=> {
      this.setState({
        width: '25%'
      })
      document.getElementById('js--cross').style.display = 'block';
  }

  closeNav = ()=> {
    this.setState({
      width: '0%'
    })

    document.getElementById('js--cross').style.display = 'none';

  }

  openModal(){
  document.getElementById("js--modal").style.display = "block";
}

closeModal = ()=> {
    document.getElementById("js--modal").style.display = "none";
}

componentDidMount(){

    if(localStorage.usertoken == null){
        console.log("komt in de if")
     document.getElementById('js--hamburger').style.display = "none";
      document.getElementById('js--meldaan').style.display = "block";
    }
    else
    {
    console.log("komt in de else")
    document.getElementById('js--hamburger').style.display = "block";
    document.getElementById('js--meldaan').style.display = "none";
    }

  }


  render(){
        return(
            <header className="BezorgtijdHeader">
                <ul className="BezorgtijdHeader_list">
                    <li className="BezorgtijdHeader_list_logo">
                        <img className="BezorgtijdHeader_list_logo_img" src={logo} alt="Logo delivery4you"/>
                    </li>
                    <li className="BezorgtijdHeader_list_tekst">

                    </li>
                    <li id="js--hamburger" onClick={this.openNav} className="BezorgtijdHeader_list_menu">
                      <section className="BezorgtijdHeader_list_menu_bar1"></section>
                      <section className="BezorgtijdHeader_list_menu_bar2"></section>
                      <section className="BezorgtijdHeader_list_menu_bar3"></section>
                    </li>
                    <li id="js--meldaan" className="BezorgtijdHeader_list_menu BezorgtijdHeader_list_aanmeld">
                    <button onClick={this.openModal} className="LandingHeader_list_login_button">Aanmelden</button>
                        <img onClick={this.openModal} className="LandingHeader_list_login_icon" src={login} alt="login icon"/>

                        <section className="LandingHeader_list_modal" id="js--modal">
                            <section className="test">
                                <p className="LandingHeader_list_modal_text">Maak een account als klant om spaarpunten te verdienen. Daarnaast kan je je registreren als restaurant of bezorger. Heb je al een account? log dan in.</p>
                                <span onClick={this.closeModal} className="LandingHeader_list_modal_header_cross">&#10005;</span>
                            </section>
                            <hr className="LandingHeader_list_modal_line"/>
                            <section className="LandingHeader_list_modal_buttons">
                                <Link to="registerklant" className="LandingHeader_list_modal_buttons_secondary">Registreer</Link>
                                <Link  to="Login" className="LandingHeader_list_modal_buttons_primary">Login</Link>
                            </section>
                        </section>
                    </li>
                </ul>
                <SideBar width={this.state.width}/>
                <span onClick={this.closeNav} id="js--cross" className="navigation_cross">&times;</span>
            </header>
        );
    }
  }

export default TestHeader;
