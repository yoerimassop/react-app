import React from 'react';
import "./WriteReviewModal.css";
import axios from "axios";
import ReactStars from "react-rating-stars-component";
import { render } from "react-dom";
import {getProfile} from "./UserFunctions";

class WriteReviewModal extends React.Component{
  constructor(props){
    super(props);
    this.state={
      naam_klant: "",
      naam_restaurant: this.props.restaurantNaam,
      rating: 0,
      review: ""
    }
    console.log(this.props.restaurantNaam);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({[e.target.name]: e.target.value })
  }

  handleSubmit = newReview => {
    console.log("review test" + newReview);
    return axios
    .post(process.env.REACT_APP_API_URL + 'api/review/storeReview', newReview, {
      headers:{"Content-Type": "application/json", "access-control-allow-origin": "*"}
    })
    .then(res => {
      console.log(res);
      window.location.href='/GerechtenKlant'
    })
    .catch(err => {
      console.log(err.response.headers);
      console.log(err.response.data);
      console.log("komt in de handlesubmit, catch");

    })
  }
  componentDidMount(){
    getProfile().then(res => {
        if(res){
          console.log(res.user.naam);
          console.log(res);
          this.setState({
              naam_klant: res.user.naam
            })
        }
        else{
          console.log('set klant naam anonymous');

          this.setState({
            naam_klant: 'anonymous'
          });
        }})
  }

  onSubmit(e){
    e.preventDefault();
    const newReview={
      naam_klant: this.state.naam_klant,
      naam_restaurant: this.state.naam_restaurant,
      rating: this.state.rating,
      review: this.state.review
    }
    console.log(newReview);

    this.handleSubmit(newReview)
    .then(
      console.log("sdffsd")
    );

  }

    render(){
        return(
          <section className="WriteReviewModalPage">
            <section className="writeReviewModal">
              <span onClick={this.props.closeWriteReviewModal} class="writeReviewModal_span">&times;</span>
              <h1 className="writeReviewModal_title">Schrijf een review</h1>
              <form className="writeReviewModal_form" onSubmit={this.onSubmit}>
                <h2 className="writeReviewModal_form_h2">Hoeveel sterren geeft u het restaurant?</h2>
                <ReactStars className="writeReviewModal_form_stars"
                   size={50}
                   half={true}
                   required
                   value={this.state.rating}
                   color2={"#FFA500"}
                   onChange={newRating => {
                     console.log(newRating);
                     this.setState({rating: newRating});
                   }}
                ></ReactStars>
                <p className="writeReviewModal_form_p">Typ hier uw review</p>
                <textarea required onChange={this.onChange} className="writeReviewModal_form_textarea" name="review" placeholder="Typ hier uw review..."></textarea>
                <button type="submit" className="writeReviewModal_form_button">Post review</button>
              </form>
            </section>
          </section>
      );
    }
}

export default WriteReviewModal;
