import React from 'react';
import DeliveryPageHeader from './DeliveryPageHeader';
import { getProfile, updateUser } from './UserFunctions';
import './EditUser.css';
import "./BezorgerPagina.css"
import icon from './images/edit.png';
import axios from 'axios';




class EditUser extends React.Component{
    constructor(){
        super();
        this.state={
            naam: '',
            email:'',
            telefoonnummer: '',
            geslacht: '',
            postcode: '',
            huisnummer: '',
            woonplaats: '',
            straatnaam: '',
            rol: '',
            color_naam:'Gainsboro',
            color_email:'Gainsboro',
            color_telefoonnummer:'Gainsboro',
            color_geslacht:'Gainsboro',
            color_postcode:'Gainsboro',
            color_huisnummer:'Gainsboro',
            color_woonplaats:'Gainsboro',
            color_straatnaam:'Gainsboro',
            readOnly_1: true,
            readOnly_2: true,
            readOnly_3: true,
            readOnly_4: true,
            readOnly_5: true,
            readOnly_6: true,
            readOnly_7: true,
            readOnly_8: true,
            hoofdtekst: '',
            subtekst: ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.updateUser = this.updateUser.bind(this);
    }

    componentDidMount() {
        
        getProfile().then(res => {
            console.log(res);
            
            this.setState({
                naam: res.user.naam,
                email: res.user.email,
                telefoonnummer: res.user.telefoonnummer,
                geslacht: res.user.geslacht,
                postcode: res.user.postcode,
                huisnummer: res.user.huisnummer,
                woonplaats: res.user.woonplaats,
                straatnaam: res.user.straatnaam,
                rol: res.user.rol
            })
        })

        if(localStorage.usertoken == null){
            console.log("komt in de if")
         document.getElementById('js--hamburger').style.display = "none";
          document.getElementById('js--meldaan').style.display = "block";
        }

        else
        {   
        console.log("komt in de else")
        document.getElementById('js--hamburger').style.display = "block";
        document.getElementById('js--meldaan').style.display = "none";
        }
        
    }

    // openModal = () => {
    //     document.getElementById("js--modal").style.display = "block";
    //   }
      
    //   closeModal = ()=> {
    //       document.getElementById("js--modal").style.display = "none";
    //   }

      closeModal = () => {
        document.getElementById("myModal").style.display = "none";
      }
    
      openModal = () => {
        document.getElementById("myModal").style.display = "block";
      }
      


     updateUser(updatedUser){
        return axios
        .patch(process.env.REACT_APP_API_URL + 'api/edituser/'+this.state.email, updatedUser, {
            headers:{'Content-Type': 'application/json'},
             _method: 'patch'
            
        })
        .then(res => {
            if(res){
                console.log(res);
                this.setState({
                    hoofdtekst: 'Je gegevens zijn juist gewijzigd!',
                    subtekst: 'wil je nog een keer je gegevens wijzigen?, herhaal dan het proces.'
                })
                this.openModal()
            }
            
            
        })
        .catch(err => {
            console.log(err);
            this.setState({
                hoofdtekst: 'Je gegevens zijn onjuist gewijzigd!',
                subtekst: 'Probeer het nog een keer en vul de juiste gegevens in.'
            })
            this.openModal()
            
        })   
    }


    changeColor_naam = ()=> {
        this.setState({
            color_naam: 'white',
            readOnly_1: false,
            
        })
    }

    changeColor_email= ()=> {
        this.setState({
            color_email: 'white',
            readOnly_2: false,
            
        })
    }

    changeColor_telefoonnummer= ()=> {
        this.setState({
            color_telefoonnummer: 'white',
            readOnly_3: false,
            
        })
    }

    changeColor_geslacht= ()=> {
        this.setState({
            color_geslacht: 'white',
            readOnly_4: false,
            
        })
    }

    changeColor_postcode= ()=> {
        this.setState({
            color_postcode: 'white',
            readOnly_5: false,
            
        })
    }

    changeColor_huisnummer= ()=> {
        this.setState({
            color_huisnummer: 'white',
            readOnly_6: false,
            
        })
    }

    changeColor_woonplaats= ()=> {
        this.setState({
            color_woonplaats: 'white',
            readOnly_7: false,
            
        })
    }

    changeColor_straatnaam= ()=> {
        this.setState({
            color_straatnaam: 'white',
            readOnly_8: false,
            
        })
    }

    onChange(e){
        this.setState({[e.target.name]: e.target.defaultValue });
    }


    onSubmit(e){
        e.preventDefault();
    
        const updatedUser = {
            naam: this.state.naam,
            email: this.state.email,
            telefoonnummer: this.state.telefoonnummer,
            geslacht: this.state.geslacht,
            postcode: this.state.postcode,
            huisnummer: this.state.huisnummer,
            woonplaats: this.state.woonplaats,
            straatnaam: this.state.huisnummer
        }
    
        console.log(updatedUser);
    
            this.updateUser(updatedUser)
            // .then(res => {      
            //     this.props.history.push('/login');
                
            // });
        
      
    }





    render(){
        console.log(this.state.naam);
        console.log(this.state.postcode);
        return(
            <section className="edit">
            <DeliveryPageHeader
                titel="Welkom op je profielpagina"
                ondertitel="Wijzig hier uw gegevens" />
                <form onSubmit={this.onSubmit} className="edit_form">

                    <section className="edit_form_header">
                        <hr className="edit_form_header_line"/>
                        <h1 className="edit_form_header_h1">Bewerk {this.state.rol}</h1>
                        <hr className="edit_form_header_line"/>
                    </section>        
                    <section className="edit_form_section edit_form_margin edit_form__responsive">
                        <label className="edit_form_section_label" htmlFor="naam">Naam</label><br/>
                        <section className="edit_form_section_icon">
                            <input name="naam" style={{ backgroundColor: this.state.color_naam }} readOnly={this.state.readOnly_1} onChange={e => this.setState({ naam: e.target.value })} className="edit_form_section_input" id="naam" value={this.state.naam} type="text"/>
                            <img onClick={this.changeColor_naam} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section edit_form_margin">
                        <label className="edit_form_section_label" htmlFor="telefoonnummer">Telefoonnummer</label><br/>
                        <section className="edit_form_section_icon">
                            <input name="telefoonnummer" style={{ backgroundColor: this.state.color_telefoonnummer }} readOnly={this.state.readOnly_3} className="edit_form_section_input" id="telefoonnummer" onChange={e => this.setState({ telefoonnummer: e.target.value })} value={this.state.telefoonnummer} type="text"/>
                            <img onClick={this.changeColor_telefoonnummer} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section">
                        <section className="edit_form_section_icon">
                            <select 
                                className="edit_form_section_input"
                                name="geslacht" 
                                id="geslacht"
                                value={this.state.geslacht}
                                style={{ backgroundColor: this.state.color_geslacht}} 
                                disabled={this.state.readOnly_4}
                                onChange={e => this.setState({ geslacht: e.target.value })}>
                                <option value="Man">Man</option>
                                <option value="Vrouw">Vrouw</option>
                                <option value="Anders">Anders</option>
                            </select>
                            <img onClick={this.changeColor_geslacht} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section">
                        <label className="edit_form_section_label" htmlFor="postcode">Postcode</label><br/>
                        <section className="edit_form_section_icon">
                            <input name="postcode" style={{ backgroundColor: this.state.color_postcode}} readOnly={this.state.readOnly_5} className="edit_form_section_input" id="postcode" onChange={e => this.setState({ postcode: e.target.value })} value={this.state.postcode} type="text"/>
                            <img onClick={this.changeColor_postcode} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section">
                        <label className="edit_form_section_label" htmlFor="huisnummer">Huisnummer</label><br/>
                        <section className="edit_form_section_icon">
                            <input name="huisnummer" style={{ backgroundColor: this.state.color_huisnummer }} readOnly={this.state.readOnly_6} className="edit_form_section_input" id="huisnummer" onChange={e => this.setState({ huisnummer: e.target.value })} value={this.state.huisnummer} type="text"/>
                            <img onClick={this.changeColor_huisnummer} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section">
                        <label className="edit_form_section_label" htmlFor="woonplaats">Woonplaats</label><br/>
                        <section className="edit_form_section_icon">
                            <input name="woonplaats" style={{ backgroundColor: this.state.color_woonplaats }} readOnly={this.state.readOnly_7} className="edit_form_section_input" id="woonplaats" onChange={e => this.setState({ woonplaats: e.target.value })} value={this.state.woonplaats} type="text"/>
                            <img onClick={this.changeColor_woonplaats} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>

                    <section className="edit_form_section">
                        <label className="edit_form_section_label" htmlFor="straatnaam">Straatnaam</label><br/>
                        <section className="edit_form_section_icon">
                            <input style={{ backgroundColor: this.state.color_straatnaam }} name="straatnaam" readOnly={this.state.readOnly_8} className="edit_form_section_input" id="straatnaam" onChange={e => this.setState({ straatnaam: e.target.value })} value={this.state.straatnaam} type="text"/>
                            <img onClick={this.changeColor_straatnaam} className="edit_form_section_icon_img" src={icon} alt="editicon"/>
                        </section>
                    </section>
                    <button className="edit_form_submit" type="submit">Wijzig gegevens</button>
                </form>
                <section onClick={this.closeModal} id="myModal" className="modal">
                    <section className="modal_content">
                    <span onClick={this.closeModal} className="close">&times;</span>
                    <p><b>{this.state.hoofdtekst}</b></p><br />
                    <p>{this.state.subtekst}</p>
                    <button onClick={this.closeModal} className="modal_content_button">Ik heb het begrepen!</button>
                </section>
            </section>
            </section>
        );
    }
}


export default EditUser;
