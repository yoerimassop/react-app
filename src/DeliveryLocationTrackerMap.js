import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import React, { Component } from 'react';

export class MapContainer extends Component {

  render() {
    let iconMarker = new window.google.maps.MarkerImage(
        "http://maps.google.com/mapfiles/kml/shapes/cycling.png",
        null,
        null,
        null,
        new window.google.maps.Size(32, 32)
    );

      return (
          <Map
            google={this.props.google}
            zoom={12}
            center={{ lat: this.props.latCustomer, lng: this.props.lngCustomer}}
            className="map"
          >
            <Marker name="jou locatie" icon={{url: "http://cdn1.iconfinder.com/data/icons/Map-Markers-Icons-Demo-PNG/256/Map-Marker-Flag--Right-Chartreuse.png", scaledSize: {width: 45, height: 45}}} position={{ lat: this.props.latCustomer, lng: this.props.lngCustomer}} />
            <Marker icon={iconMarker} position={{ lat: this.props.latDelivery, lng: this.props.lngDelivery}} />
          </Map>
      )
    };
}


  export default GoogleApiWrapper({
  apiKey: 'AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc'
})(MapContainer);
