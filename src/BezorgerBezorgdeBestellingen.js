import React from 'react'
import "./BezorgerCard.css"
import BezorgerFilter from './BezorgerFilter'
import axios from 'axios';
import {getProfile} from "./UserFunctions";
import RestaurantBezorgDatum from './restaurant/RestaurantBezorgDatum.js';

class BezorgerBezorgdeBestellingen extends React.Component{
  constructor() {
    super()
    this.state = {
        bestellingen : []
    }
  }

  componentDidMount() {

    getProfile().then(res => {

      let email = res.user.email;

      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/bezorger/bezorgd/' + email, {
      })
      .then(response => {
          this.setState({bestellingen: response.data})
      })
      .catch(err => {
          console.log(err)
      })
    })
  }

  updateStateBestellingen = () => {

    getProfile().then(res => {

      let email = res.user.email;

      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/bezorger/bezorgd/' + email, {
      })
      .then(response => {
          this.setState({bestellingen: response.data})
      })
      .catch(err => {
          console.log(err)
      })
    })
  }

  onSubmit = (filter) => {

    getProfile().then(res => {

      let email = res.user.email;

      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/bezorger/bezorgd/' + email + "/" + filter, {
      })
      .then(response => {
          this.setState({bestellingen: response.data})
      })
      .catch(err => {
          console.log(err)
      })
    })
  }

  render(){
    const { bestellingen } = this.state;

    return (
      <article className="card">
        <header className="card__header">
          <h2>Bezorgde Bestellingen</h2>
        </header>
        <section className="card__content">
        <BezorgerFilter onSubmit={this.onSubmit}/>
          <ul className="card__content__list">
            {
            bestellingen.length ?
            bestellingen.reverse().map(bestelling =>
            <li key={bestelling.ordernummer} className="card__content__list__item">
            <p><b>Ordernummer:</b> {bestelling.ordernummer}</p>
            <p><b>Restaurant:</b> {bestelling.naam_restaurant}</p>
            <p><b>Bezorgd op:</b> {bestelling.bezorgadres}</p>
            <p><span><b>Bezorgdatum:</b></span> <RestaurantBezorgDatum bezorgdatum={bestelling.bezorgdatum}/></p>
            </li>)
            :<li className="card__content__list__item">
              <p className="card__content__list__item__tekst">Geen bestelling bezorgd in deze periode</p>
            </li>
            }
          </ul>
        </section>
      </article>
    );
  }
}

export default BezorgerBezorgdeBestellingen;
