import React from "react";
import {getProfile} from "./UserFunctions"
import "./BezorgLocatie.css"



class BezorgLocatie extends React.Component {

    constructor() {
      super()
        this.state = {
          naam: '',
          straatnaam: '',
          woonplaats: '',
          huisnummer: ''
        }
    }

    componentDidMount() {

      getProfile().then(response => {

          this.setState({
              naam: response.user.naam,
              straatnaam: response.user.straatnaam,
              woonplaats: response.user.woonplaats,
              huisnummer: response.user.huisnummer
          })
      })
    }

  render() {
    return (
      <section className="bezorglocatie">
        <article className="bezorglocatie__article">
          <header className="bezorglocatie__article__header">
            <h3>Bezorgadres</h3>
          </header>
          <p className="bezorglocatie__article__text">{this.state.naam || "naam"}</p>
          <p className="bezorglocatie__article__text">{this.state.straatnaam || "straatnaam"}</p>
          <p className="bezorglocatie__article__text">{this.state.huisnummer || "huisnummer"}</p>
          <p className="bezorglocatie__article__text">{this.state.woonplaats || "woonplaats"}</p>
        </article>
      </section>
  );
  }
}

export default BezorgLocatie;
