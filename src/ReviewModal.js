import React from 'react';
import Review from "./Review.js";
import "./ReviewModal.css";
import axios from "axios";
class ReviewModal extends React.Component{
  state={averageRating: 0, reviews: [], restaurant: ""};
  constructor(props){
    super(props);
    this.writeReviewModalRef = React.createRef();
    this.showWriteReviewModal = this.showWriteReviewModal.bind(this);
  }

  showStarsInReview(rating){
    if(rating <= 0){
      return(
          <section className="reviewModal_gemiddeldeBeoordeling_stars">
            <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
            <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
            <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
            <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
            <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          </section>
        )
      }
    else if(rating <= 1){
      return(
        <section className="reviewModal_gemiddeldeBeoordeling_stars">
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
        </section>
      )
    }
    else if(rating <= 2){
      return(
        <section className="reviewModal_gemiddeldeBeoordeling_stars">
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
        </section>
      )
    }
    else if(rating <= 3){
      return(
        <section className="reviewModal_gemiddeldeBeoordeling_stars">
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
        </section>
      )
    }
    else if(rating <= 4){
      return(
        <section className="reviewModal_gemiddeldeBeoordeling_stars">
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star reviewModal_gemiddeldeBeoordeling_stars_star"></span>
        </section>
      )
    }
    else if(rating <= 5){
      return(
        <section className="reviewModal_gemiddeldeBeoordeling_stars">
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
          <span className="fa fa-star checked reviewModal_gemiddeldeBeoordeling_stars_star"></span>
        </section>
      )
    }
  }

  showAllReviews(){
    const reviews = [];
    if(this.state.reviews.length > 0){
      for(let i = 0; i < this.state.reviews.length; i++){
        reviews.push(<Review name={this.state.reviews[i].naam_klant} review={this.state.reviews[i].review} stars={this.showStarsInReview(this.state.reviews[i].rating)}/>)
      }
    }else{
      return <h3 className="ReviewModal_h3">Er zijn nog geen reviews, schrijf een review om de eerste te zijn!</h3>
    }

    return reviews;
  }

  updateReviews(){
    axios.get(process.env.REACT_APP_API_URL + 'api/review/' + this.props.restaurantNaam).then(res => {
      if(res.data === undefined){
        console.log("geen review beschikbaar");
      }else{
        this.setState({reviews: res.data}, function(){
          this.updateAverageRating()
        });
      }
      this.setState({restaurant: this.props.restaurantNaam})
    })
  }

  updateAverageRating(){
      let stars = 0;
      this.state.reviews.forEach(element => stars = stars + element.rating)
      if(this.state.reviews.length === 0){
        this.setState({averageRating: 0})
      }else{
        this.setState({averageRating: (stars / this.state.reviews.length).toFixed(1)})
      }
  }

  showWriteReviewModal(restaurant){
    this.props.showWriteReviewModal(restaurant);
  }

  showAverageRatingStars(){
    return this.showStarsInReview(this.state.averageRating);
  }

  componentDidMount(){
    this.updateReviews()
  }

    render(){

      let button;
      if(this.props.showButton === 'false'){
        
      }else{
        button = <button onClick={this.props.showWriteReviewModal} className="reviewModal_button">Schrijf een review</button>
      }

      console.log(this.state.averageRating);
        return(
          <section className="reviewModalPage">
            <section className="reviewModal">
              <header className="reviewModal_header">
                <h1>{this.state.restaurant}</h1>
                <span onClick={this.props.closeReviewModal} class="reviewModal_header_span">&times;</span>
              </header>
              <section className="reviewModal_gemiddeldeBeoordeling">
                <h2>Gemiddelde Beoordeling</h2>
                <h3>{this.state.averageRating} / 5</h3>
                <section className="reviewModal_gemiddeldeStars">
                  {this.showAverageRatingStars()}
                </section>
              </section>
              {this.showAllReviews()}
              {button}
            </section>
          </section>
      );
    }
}

export default ReviewModal;
