import React from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import LandingspageHeader from "./LandingspageHeader.css";

class AdresSearchBar extends React.Component {
  render() {

    return(
    <section>
    <form className="LandingHeader_list_searchbar_form" action="" onSubmit={this.props.onAdressSubmit}>
        {/* <input className="LandingHeader_list_searchbar_form_input" type="text" placeholder="Vul uw postcode in"/> */}

    <PlacesAutocomplete
            value={this.props.value}
            onChange={this.props.onChange}
            onSelect={this.props.onSelect}
            searchOptions={this.props.searchOptions}
            
          >
            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
              <section className="LandingHeader_list_searchbar_form_section">
                <input
                  type="submit"
                  {...getInputProps({
                    placeholder: 'Vul je postcode in',
                    className: 'LandingHeader_list_searchbar_form_section_input',
                  })}
                />
                  <div className="autocomplete-dropdown-container">
                    {loading && <div>Loading...</div>}
                    {suggestions.map(suggestion => {
                      const className = suggestion.active
                        ? 'suggestion-item--active'
                        : 'suggestion-item';
                      // inline style for demonstration purpose
                      const style = suggestion.active
                        ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                        : { backgroundColor: '#ffffff', cursor: 'pointer' };
                      return (
                          <div
                            {...getSuggestionItemProps(suggestion, {
                              className,
                              style,
                            })}
                          >
                            <span className="autocomplete_suggesties">{suggestion.description}</span>
                          </div>
                      );
                    })}
                  </div>
              </section>
            )}
          </PlacesAutocomplete>
          <button  className="LandingHeader_list_searchbar_form_button" type="submit">Zoek restaurants</button>
        </form>
        </section>
    );
  }
}

export default AdresSearchBar
