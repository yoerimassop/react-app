import React from 'react';
import "./LandingspageHeader.css";
import logo from './images/logo.png';
import login from './images/login.png';
import {Link, withRouter} from 'react-router-dom';
import SideBar from './SideBar';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {adresActie} from './actions';
import AdresSearchBar from './AdresSearchBar.js';

class LandingspageHeader extends React.Component{

    constructor(props){
        super(props);
        this.goPage = this.goPage.bind(this);
        this.state = {
            address: '',
            width: '',
            display: 'none',
            firstpage: 'nee',
            hamburger: 'false'
        }
    }

    handleChange = address => {
        this.setState({
            address
        });
    }

    searchOptions = {
        componentRestrictions: { country: ['nl'] }
      }

    openModal = () => {
        document.getElementById("js--modal").style.display = "block";
    }

    closeModal = () => {
        document.getElementById("js--modal").style.display = "none";
    }

    goPage =(e)=> {
        e.preventDefault();
        this.props.history.push('/login');
    }


    
 
    componentDidMount(){   
      if(localStorage.getItem('usertoken')== null){
          console.log("er is geen usertoken")
          document.getElementById('js--hamburger').style.display = "none";
          document.getElementById('js--meldaan').style.display = "block";
      }
      else
      {
        console.log("er is wel een usertoken en de hamburger moet komen")
        console.log(localStorage.getItem('usertoken')); 
        document.getElementById('js--hamburger').style.display = "block";
        document.getElementById('js--meldaan').style.display = "none";
      }

    }

    openNav = ()=> {
      this.setState({
        width: '350px'
      })
      document.getElementById('js--cross').style.display = 'block';
    }

  closeNav = ()=> {
    this.setState({
      width: '0px'
    })
    document.getElementById('js--cross').style.display = 'none';
  }

  
  onAdressSubmit = (e) =>{
    e.preventDefault();
    this.props.adresActie(this.state.address);
    this.props.history.push('/KlantBestellen');
  }




    render(){
      console.log(this.state.firstpage);
        return(
            <header id="landing" className="LandingHeader">
                <ul  className="LandingHeader_list">
                    <li className="LandingHeader_list_logo">
                        <Link to="">
                             <img className="LandingHeader_list_logo_img" src={logo} alt="Logo delivery4you"/>
                        </Link>
                    </li>
                    <li className="LandingHeader_list_tekst">
                        <p className="LandingHeader_list_tekst_phoofd">Wil jij de lokale ondernemer steunen!</p><br/>
                        <p className="LandingHeader_list_tekst_p">Zoek lokale restaurants in de buurt.</p>
                    </li>

                    <li id="js--hamburger" onClick={this.openNav} className="LandingHeader_list_hamburger BezorgtijdHeader_list_menu">
                      <section className="BezorgtijdHeader_list_menu_bar1"></section>
                      <section className="BezorgtijdHeader_list_menu_bar2"></section>
                      <section className="BezorgtijdHeader_list_menu_bar3"></section>
                    </li>

                    <li id="js--meldaan" className="LandingHeader_list_login">
                        <button onClick={this.openModal} className="LandingHeader_list_login_button">Aanmelden</button>
                        <img onClick={this.openModal} className="LandingHeader_list_login_icon" src={login} alt="login icon"/>

                        <section className="LandingHeader_list_modal" id="js--modal">
                            <section className="test">
                                <p className="LandingHeader_list_modal_text">Maak een account als klant om je gegevens te onthouden. Daarnaast kan je je registreren als restaurant of bezorger. Heb je al een account? log dan in.</p>
                                <span onClick={this.closeModal} className="LandingHeader_list_modal_header_cross">&#10005;</span>
                            </section>
                            <hr className="LandingHeader_list_modal_line"/>
                            <section className="LandingHeader_list_modal_buttons">
                                <Link to="registerklant" className="LandingHeader_list_modal_buttons_secondary">Registreer</Link>
                                <Link  to="Login" className="LandingHeader_list_modal_buttons_primary">Login</Link>
                            </section>
                        </section>

                    </li>
                    <li className="LandingHeader_list_searchbar">
                    {console.log(this.state.address)}
                        <AdresSearchBar classinput="LandingHeader_list_searchbar_form"
                        onAdressSubmit={this.onAdressSubmit}
                        value={this.state.address}
                        onChange={this.handleChange}
                        onSelect={this.handleSelect}
                        searchOptions={this.searchOptions}
                        />
                    </li>
                </ul>
                <SideBar display={this.state.display} width={this.state.width}/>
                <span onClick={this.closeNav} id="js--cross" className="navigation_cross">&times;</span>
            </header>
        );
    }
}


const mapStateToProps = state => {
	return {adresKlantBestelling: state.adresKlantBestelling};
};

const mapDispatchToProps = {
  adresActie
}

export default connect(mapStateToProps,
  mapDispatchToProps
)(withRouter(LandingspageHeader));
