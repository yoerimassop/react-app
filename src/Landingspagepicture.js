import React from 'react';
import "./Landingspagepicture.css";
import image from './images/foodheader.jpg';
import image2 from './images/homepage_responsive.png';
import {Link} from 'react-scroll';







const Landingspagepicure = (props) => {
    
        const omlaag = ()=> {
                document.getElementById("js--arrow3").style.visibility = "visible";
                document.getElementById("js--arrow2").style.visibility = "hidden";
        }

        const omhoog = () => {
            document.getElementById("js--arrow2").style.visibility = "visible";
            document.getElementById("js--arrow3").style.visibility = "hidden";
            document.getElementById("js--arrow2").style.transform = "rotate(0deg)";
            console.log("in functie omhoog");       
        }

    
    return(
        <section className="mainpicture">
            <article className="mainpicture_article">
                <p className="mainpicture_article_big">Bestel makkelijk en eenvoudig een gerecht</p><br/>
                <p className="mainpicture_article_small">En steun hiermee de lokale ondernemer</p>
            </article>
            <figure className="mainpicture_figure">
                <img className="mainpicture_figure_img mainpicture_figure_img__desktop" src={image} alt="foto van eten"/>
                <img className="mainpicture_figure_img mainpicture_figure_img__responsive" src={image2} alt="foto van eten"/>
            </figure>
            <section id="click" className="mainpicture_section  mainpicture_bounce">
                <Link onClick ={omlaag} id="js--arrow2" to="info" offset={-160} smooth={true} duration={1000} class="fa fa-angle-double-down fa-3x"></Link>
                
          </section>
          <section  className="mainpicture_section  extradiv mainpicture_bounce">
            <Link onClick ={omhoog} id="js--arrow3" to="mainpicture" offset={-350} smooth={true} duration={1000} class="fa extra fa-angle-double-down fa-3x"></Link>
          </section>
        </section>
    );
    }

export default Landingspagepicure;