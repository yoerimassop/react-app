import React from 'react';
import './WinkelmandjeItem.css';
import prullenbak from "./images/delete.svg";

class WinkelmandjeItem extends React.Component{
  deleteItem = () =>{
    this.props.deleteItem(this.props.nummer)
  }
  render(){
    return(
      <section className="WinkelmandjeItem">
        <li className="WinkelmandjeItem_items">
          <p className="WinkelmandjeItem_items_aantal">{this.props.aantal || '2x'}</p>
          <p className="WinkelmandjeItem_items_gerecht">{this.props.gerecht || 'MCnogwat'}</p>
          <p className="WinkelmandjeItem_items_prijs">{this.props.prijs || '10.99'}€</p>
          <img onClick={this.deleteItem} className="WinkelmandjeItem_delete" src={prullenbak} alt="Verwijder dit Item"/>
        </li>
      </section>
    );
  }
}

export default WinkelmandjeItem
