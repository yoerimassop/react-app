import React from 'react';
import './GerechtenKlantBanner.css';
import GerechtenRestaurantCard from './GerechtenRestaurantCard';

class GerechtenKlantBanner extends React.Component{
  constructor(props){
    super(props);
    console.log(props);
    this.showReviewModal = this.showReviewModal.bind(this);
  };

  showReviewModal(naam){
    this.props.showReviewModal(naam);
  }

  render(){
    const BASE_URL_CATEGORIE_IMG = process.env.REACT_APP_API_URL + 'images/categorie_image/'
    /* <GerechtRestaurantCard title={this.props.gekozenRestaurant || "Jouw favoriete Restaurant"}/> */
    return(
      <section className="GerechtenKlantBanner">
        <GerechtenRestaurantCard showReviewModal={this.showReviewModal} naam={this.props.naam} logo={this.props.logo}
        rating={this.props.rating} beschrijving={this.props.beschrijving}/>
        <img className="GerechtenKlantBannerImg" src={BASE_URL_CATEGORIE_IMG + this.props.categorie + "Gerecht.jpg"} alt="Banner Restaurant"/>
      </section>
    );
  }
}

export default GerechtenKlantBanner
