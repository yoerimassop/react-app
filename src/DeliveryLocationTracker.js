import React from 'react';
import ReactDOM from "react-dom";
import GoogleMap from 'google-maps-react';
import "./DeliveryLocationTracker.css";
import DeliveryLocationTrackerMap from "./DeliveryLocationTrackerMap";
import Geocode from "react-geocode";
import { Geolocated } from "react-geolocated";
import axios from "axios";

class DeliveryLocationTracker extends React.Component{
    state = {deliveryStatus: "Nog niet vertrokken",
            bezorger: "",
            locationDelivery: "",
            locationCustomer: "",
            latCustomer: 0.0,
            lngCustomer: 0.0,
            latDelivery: 0.0,
            lngDelivery: 0.0};

    setLatAndLngCustomer(){
      Geocode.setApiKey("AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc");
      Geocode.fromAddress(this.state.locationCustomer).then(
        response => {
         const { lat, lng } = response.results[0].geometry.location;
         this.setState({latCustomer: lat, lngCustomer: lng})
        },
       error => {
         console.error(error);
       }
      )
    }

    setLatAndLngDelivery(){
      Geocode.setApiKey("AIzaSyAaXREgifgVpZsMR_gCN7UPnqfliuBI4wc");
      Geocode.fromAddress(this.state.locationDelivery).then(
        response => {
         const { lat, lng } = response.results[0].geometry.location;
         this.setState({latDelivery: lat, lngDelivery: lng})
        },
       error => {
         console.error(error);
       }
      )
    }

    updateLocationDelivery(){
      axios.get(process.env.REACT_APP_API_URL + 'api/bezorger/' + this.state.bezorger).then(res => {
        this.setState({locationDelivery: res.data.location}, function(){
          this.setLatAndLngDelivery()
        })
      })
    }

    updateLocationCustomer(){
      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/' + this.props.ordernummer).then(res => {
        this.setState({deliveryStatus: res.data.status});
        this.setState({bezorger: res.data.email_bezorger});
        this.setState({locationCustomer: res.data.bezorgadres}, function(){
          this.setLatAndLngCustomer()
          this.updateLocationDelivery()
        })
      })
    }

    componentDidMount(){
      this.updateLocationCustomer()
    };


    render(){
      return(
        <section className="DeliveryLocationTracker">
          <section className="DeliveryLocationTracker_text">
            <h2>Status bezorger: {this.state.deliveryStatus}</h2>
            <p>Wanneer de bezorger vertrekt, kun je hem live tracken!</p>
          </section>
          <DeliveryLocationTrackerMap
          latCustomer={this.state.latCustomer}
          lngCustomer={this.state.lngCustomer}
          latDelivery={this.state.latDelivery}
          lngDelivery={this.state.lngDelivery}>
          </DeliveryLocationTrackerMap>
        </section>
      )
    };
}

export default DeliveryLocationTracker;
