import React from 'react';
import ReactDOM from "react-dom";
import "./DeliveryTimeTracker.css";
import axios from "axios";
import bezorger_image from './images/bezorger.jpg';

class DeliveryTimeTracker extends React.Component{
  state = {deliveryTime: "", deliveryDate: "", MinutesTillDelivery: 0};


//  makeApiCall = searchTerm => {
//      const BASE_URL = "";
//      axios.get(BASE_URL + bestelling/orderNumber).then(res => {
//        this.setState({
//          deliveryTime: res.data.bezorgdatum,
//          deliveryDate: res.data.bezorgtijd
//        });
//      });
//    }
  makeApiCall(){
    axios.get(process.env.REACT_APP_API_URL + "api/bestelling/" + this.props.ordernummer).then(res => {
      this.setState({deliveryTime: res.data.bezorgtijd , deliveryDate: res.data.bezorgdatum}, () => {this.updateMinutesTillDelivery();})
    })
  }

  updateMinutesTillDelivery(){
    let splittedDeliveryTime = this.state.deliveryTime.split(':')
    let splittedDeliveryDate = this.state.deliveryDate.split('-')
    let hour = splittedDeliveryTime[0];
    let minute = splittedDeliveryTime[1];
    let month = splittedDeliveryDate[1];
    let day = splittedDeliveryDate[2] - 1;
    let year = splittedDeliveryDate[0];

    let currentTime = new Date();
    let deliveryTime = new Date();
    deliveryTime.setFullYear(year);
    deliveryTime.setMonth(month - 1);
    deliveryTime.setDate(day + 1);
    deliveryTime.setMinutes(minute);
    deliveryTime.setHours(hour);

    //Dit is in milliseconden /60000 om naar minuten te gaan
    let minutesTillDelivery = Math.round((deliveryTime - currentTime) / 60000);

    if(minutesTillDelivery < 0){
      this.setState({MinutesTillDelivery: 0});
    }
    else{
      this.setState({MinutesTillDelivery: minutesTillDelivery});
    }
  }

  componentDidMount(){
    this.makeApiCall()
  };

    render(){
      var styling =  ((30-this.state.MinutesTillDelivery) / 30) * 1030;
      if(styling > 1030){
        styling = 1030
      }
      if(styling < 0){
        styling = 0
      }
      const style = {
        left: styling
      };

        return(
          <section className="BezorgtijdTracker">
            <section className="BezorgtijdTracker_text">
              <h2>Bezorgtijd over:</h2>
              <h1>{this.state.MinutesTillDelivery} minuten</h1>
            </section>
            <figure className="BezorgtijdTracker_bezorger">
              <img style={style} className="BezorgtijdTracker_bezorger_img" src={bezorger_image} alt="Delivery guy on a scooter"/>
            </figure>
          </section>
        );
    }
}

export default DeliveryTimeTracker;
