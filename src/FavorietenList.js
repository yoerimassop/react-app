import React from "react";
import TestHeader from "./TestHeader";
import logo from "./images/logo.png";
import registerFavoriet from "./UserFunctions";
import {Link, withRouter} from 'react-router-dom';

import axios from 'axios';

import "./FavorietenList.css";


class FavorietenList extends React.Component{

  constructor() {
    super()
      this.state = {
        favorieten: [],
        naam_restaurant: '',
        betaalmethode: ''
      }
  }

  componentDidMount() {
    axios.get('http://localhost:8000/favoriet', {})
    .then(response => {
        console.log(response)
        this.setState({
          favorieten: response.data})
    })
    .catch(error => {
      console.log(error);
    })
 }

  render(){
    const favorieten = this.state;

    return (
      <section>
        <TestHeader />
        <article className="favoriet">
          <header className="favoriet__header">
            <h2>Uw favorieten</h2>
          </header>
          <section className="favoriet__content">
            <ul className="favoriet__content__list">
              {this.state.favorieten.map(favoriet => (
                <li className="favoriet__content__list__item" key={favoriet.id}>
                  <p className="favoriet__content__list__item__text">{favoriet.naam}</p>
                  <figure className="favoriet__content__list__item__figure">
                    <img src={logo} className="favoriet__content__list__item__figure__img"/>
                  </figure>
                  <button className="favoriet__content__list__item__button"><Link className="favoriet__button__link" to={"KlantBestellen/" + favoriet.naam}>Bekijk het menu</Link></button>
                </li>
              ))}
            </ul>
          </section>
        </article>
      </section>
    );
  }
}

export default FavorietenList;


// this.state.data.map(function(item, i){
//   console.log('test');
//   return <li key={i}>Test</li>
// })
