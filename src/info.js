import React from 'react';
import './info.css';
import logo1 from './images/container1.png';
import logo2 from './images/container2.png';
import logo3 from './images/container3.png';
import {Link} from 'react-scroll';


class info extends React.Component{

    
    render(){

        
        return(
            <section id="info" className="infocontainer">
                <section className="infocontainer_head">
                    <h1 className="infocontainer_head_h1">zo werkt delivery4you.nl</h1>
                    <p className="infocontainer_head_p">heel makkelijk</p>
                </section>
                <ul className="infocontainer_list">
                    <li className="infocontainer_list_item">
                        <img className="infocontainer_list_item_img" src={logo1} alt=""/>
                        <h2 className="infocontainer_list_item_h2">Restaurants in de buurt</h2>
                        <p className="infocontainer_list_item_p">Door in de zoekbalk de postcode in te vullen ziet u de lokale restaurants in de buurt.</p>
                        <div  className="infocontainer_list_item_arrow">
                         <Link id="js--arrow3" to="info" offset={365} smooth={true} duration={1000} class="infocontainer_list_item_arrow_down fa fa-angle-double-down fa-3x"></Link>
                        </div>
                    </li>
                    <li className="infocontainer_list_item">
                        <img className="infocontainer_list_item_img" src={logo2} alt=""/>
                        <h2 className="infocontainer_list_item_h2">Kies het gerecht</h2>
                        <p className="infocontainer_list_item_p">Wanneer het restaurant gekozen is kunt u het eten van het menu toevoegen aan uw winkelwagen.</p>
                        <div  className="infocontainer_list_item_arrow">
                         <Link id="js--arrow3" to="info" offset={965} smooth={true} duration={1000} class="infocontainer_list_item_arrow_down fa fa-angle-double-down fa-3x"></Link>
                        </div>            
                    </li>
                    <li className="infocontainer_list_item">
                        <img className="infocontainer_list_item_img"  src={logo3} alt=""/>
                        <h2 className="infocontainer_list_item_h2">Betaal en wacht</h2>
                        <p className="infocontainer_list_item_p">Wanneer de bestellingen in het winkelwagentje zitten kunt u de bestelling afrekenen en de bezorger tracken.</p>
                    </li>
                </ul>
            </section>
        );
    }
}

export default info;