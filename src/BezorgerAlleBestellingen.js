import React from 'react'
import "./BezorgerCard.css"
import BezorgerZoekbalk from './BezorgerZoekbalk'
import axios from 'axios';


class BezorgerAlleBestellingen extends React.Component{
  constructor() {
    super()
    this.state = {
      bestellingen: [],
      search:''
    }
  }

  componentDidMount() {
    
    axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/status/gemaakt', {
    })
    .then(response => {
        console.log(response)
        this.setState({bestellingen: response.data})
    })
    .catch(err => {
        console.log(err)
    })
 }

 updateStateBestellingen = () => {
    
  axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/status/gemaakt', {
  })
  .then(response => {
      console.log(response)
      this.setState({bestellingen: response.data})
  })
  .catch(err => {
      console.log(err)
  })
}

  onSubmit = (searchTerm) => {

    this.setState({search: searchTerm});
  }

 
 
  render(){ 
    let filteredBestellingen = this.state.bestellingen.filter(
      (bestelling) => {
        return bestelling.naam_restaurant.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    );
    return (
      <article className="card">
        <header className="card__header">
          <h2>Alle Bestellingen</h2>
        </header>
        <section className="card__content">
          <BezorgerZoekbalk placeholder="Zoek op restaurant..." onSubmit={this.onSubmit}/>
          <ul className="card__content__list">
          {
            filteredBestellingen.length ?
            filteredBestellingen.reverse().map(bestelling =>
            <li key={bestelling.ordernummer} className="card__content__list__item">
            <p><b>Ordernummer:</b> {bestelling.ordernummer}</p>
            <p><b>Restaurant:</b> {bestelling.naam_restaurant}</p>
            <p><b>Bezorgen op:</b> {bestelling.bezorgadres}</p>
            <p><b>Bezorgtijd:</b> {bestelling.bezorgtijd}</p>
            <button onClick={() => this.props.updateStatus(bestelling.ordernummer, "geclaimd", bestelling.naam_restaurant)} className="card__content__list__button">Bestelling claimen</button> 
            </li>)
            :<li className="card__content__list__item">
              <p className="card__content__list__item__tekst">Geen bestelling beschikbaar</p>
            </li>
            }
          </ul>
        </section>
      </article>
    );
  }
}

export default BezorgerAlleBestellingen;