import React from 'react'
import "./BezorgerCard.css"
import axios from 'axios';
import {getProfile} from "./UserFunctions";

class BezorgerGeclaimdeBestellingen extends React.Component{
  constructor() {
    super()
    this.state = {
        bestellingen: []
    }
  }

  componentDidMount() {
    getProfile().then(res => {
      console.log(res);
      
      let email = res.user.email;

      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/bezorger/geclaimd/' + email, {
      })
      .then(response => {
          this.setState({bestellingen: response.data})
      })
      .catch(err => {
          console.log(err)
      })
    })
  }
  
  updateStateBestellingen = () => {
    
    getProfile().then(res => {
      console.log(res);
      
      let email = res.user.email;

      axios.get(process.env.REACT_APP_API_URL + 'api/bestelling/bezorger/geclaimd/' + email, {
      })
      .then(response => {
          this.setState({bestellingen: response.data})
      })
      .catch(err => {
          console.log(err)
      })
    })
  }

  render(){
    const { bestellingen } = this.state;

    return (
      <article className="card">
        <header className="card__header">
          <h2>Geclaimde Bestellingen</h2>
        </header>
        <section className="card__content">
          <ul className="card__content__list">
          {
            bestellingen.length ?
            bestellingen.reverse().map(bestelling =>
            <li key={bestelling.ordernummer} className="card__content__list__item">
            <p><b>Ordernummer:</b> {bestelling.ordernummer}</p>
            <p><b>Restaurant:</b> {bestelling.naam_restaurant}</p>
            <p><b>Bezorgen op:</b> {bestelling.bezorgadres}</p>
            <p><b>Bezorgtijd:</b> {bestelling.bezorgtijd}</p>
            <button onClick={() => this.props.updateStatus(bestelling.ordernummer, "bezorgd", bestelling.naam_restaurant)} className="card__content__list__button">Bestelling bezorgd</button> 
            </li>)
            :<li className="card__content__list__item">
              <p className="card__content__list__item__tekst">Geen bestelling geclaimd</p>
            </li>
          }
          </ul>
        </section>
      </article>
    );
  }
}

export default BezorgerGeclaimdeBestellingen;