import React from 'react';
// import {register} from './UserFunctions.js';
import LandingspageHeader from './LandingspageHeader.js';
import {Link, withRouter} from 'react-router-dom';
import axios from 'axios';
import "./RegisterRestaurant.css";
import {login} from './UserFunctions.js';



class RegisterRestaurant extends React.Component{
    constructor(){
        super();
        this.state = {
            first_name:'',
            last_name:'',
            email: '',
            telefoonnummer:'',
            password: '',
            herhalendwachtwoord: '',
            geslacht:'man',
            postcode:'',
            huisnummer:'',
            woonplaats:'',
            straatnaam:'',
            geboortedatum:'',
            rol:'restaurant',
            errors: {},
            kleur:'white',
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
    }

    onChange(e){
    this.setState({[e.target.name]: e.target.value })

}

onChangePassword(e){

    if(document.getElementById('js--password').value != document.getElementById('js--conformpassword').value) {
      document.getElementById('js--conformpassword').setCustomValidity("Passwords Don't Match");
    } else {
      document.getElementById('js--conformpassword').setCustomValidity('');
    }

    this.setState({[e.target.name]: e.target.value });

  }






onSubmit(e){
    e.preventDefault();

    const newUser = {
        naam: this.state.first_name + " " + this.state.last_name,
        email:this.state.email,
        telefoonnummer: this.state.telefoonnummer,
        password: this.state.password,
        geslacht: this.state.geslacht,
        postcode: this.state.postcode,
        huisnummer: this.state.huisnummer,
        woonplaats: this.state.woonplaats,
        straatnaam: this.state.straatnaam,
        geboortedatum: this.state.geboortedatum,
        rol: this.state.rol
    }

    console.log(newUser);





    try {
    this.register(newUser)
    login(newUser).then(res=> {
        if(res){
            this.props.history.push('/registerrestaurant2')
        }
        else
        {
            this.setState({
                kleur: 'red'
            })
        }
    });

    } catch (e) {
        console.log(e);

    }
}


register = newUser => {
    return axios
    .post(process.env.REACT_APP_API_URL + 'api/register', newUser, {
        headers:{'Content-Type': 'application/json'}
    })
    .then(res => {
        console.log("komt in de then")
        if(res){
            console.log(res);
            this.props.history.push('/registerrestaurant2');
        }
        else
        {
            this.setState({
                kleur:'red'
            })
        }
    })
    .catch(err => {
        this.setState({
            kleur:'red'
        })
        console.log(err);

    })
}

render(){
    return(
    <section>
        <LandingspageHeader/>
        <section className="register">
        <section className="register_back">
                <Link to="" className="register_back_arrow">&#8592;</Link>
            </section>
            <form className="register_form"  onSubmit={this.onSubmit}>
                <section className="register_form_header">
                    <hr className="register_form_header_line"/>
                    <section>
                         <h1 className="register_form_header_h1">Registreer restaurant</h1>
                         <p className="register_form_header_p">Stap 1: privegegevens</p>
                    </section>
                    <hr className="register_form_header_line"/>
                </section>

            <section className="gridsection">
                <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="first_name">Voornaam</label><br/>
                <input id="js--input1" className="register_form_input" type="text"
                 name="first_name"
                 placeholder="Typ hier de voornaam"
                 value={this.state.first_name}
                 onChange={this.onChange}
                 required
                />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="last_name">Achternaam</label>
                    <input className="register_form_input" type="text"
                    id="js--input2"
                    name="last_name"
                    placeholder="Typ hier de achternaam"
                    value={this.state.last_name}
                    onChange={this.onChange}
                    required
                    />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="email"> Prive emailadres</label>
                    <input type="email"
                    className="register_form_input"
                    name="email"
                    placeholder="Typ hier de mail"
                    value={this.state.email}
                    onChange={this.onChange}
                    required
                    />
                    <small className="register_form_small__error" style={{ color: this.state.kleur}}>Emailadres al in gebruik</small>
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="telefoonnummer">Prive telefoonnummer</label>
                    <small className="register_form_small">Format: 06-12345678</small>
                    <input type="tel"
                    className="register_form_input"
                    name="telefoonnummer"
                    placeholder="06-12345678"
                    value={this.state.telefoonnummer}
                    onChange={this.onChange}
                    pattern="[0-6]{2}-[0-9]{8}"
                    required
                    />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="password">Wachtwoord</label>
                    <input type="password"
                    className="register_form_input"
                    name="password"
                    placeholder="Typ hier het wachtwoord"
                    value={this.state.password}
                    onChange={this.onChange}
                    id="js--password"
                    required
                    minLength="6"
                    />
                </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="password">Herhaal wachtwoord</label>
                <input type="password"
                 className="register_form_input"
                 name="herhaaldpassword"
                 placeholder="Bevestig wachtwoord"
                 id="js--conformpassword"
                 onKeyUp={this.onChangePassword}
                 required
                 minLength="6"
                />
            </section>

            <section>
                <p className="register_form_star">*</p>
                <label className="register_form_label" htmlFor="Geslacht">Geslacht</label>
                <select
                className="register_form_select"
                name="geslacht"
                id="Geslacht"
                value={this.state.geslacht}
                onChange={this.onChange}
                >
                    <option value="man">Man</option>
                    <option value="vrouw">Vrouw</option>
                    <option value="anders">Anders</option>
                </select>
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="postcode">Postcode</label>
                    <input type="text"
                    className="register_form_input"
                    name="postcode"
                    placeholder="Typ hier de postcode"
                    value={this.state.postcode}
                    onChange={this.onChange}
                    required
                    maxLength="6"
                    />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="huisnummer">Huisnummer</label>
                    <input type="text"
                    className="register_form_input"
                    name="huisnummer"
                    placeholder="Typ hier het huisnummer"
                    value={this.state.huisnummer}
                    onChange={this.onChange}
                    required
                    maxLength="4"
                    />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="woonplaats">Woonplaats</label>
                    <input type="text"
                    className="register_form_input"
                    name="woonplaats"
                    placeholder="Typ hier de woonplaats in"
                    value={this.state.woonplaats}
                    onChange={this.onChange}
                    required
                    />
                </section>

                <section>
                    <p className="register_form_star">*</p>
                    <label className="register_form_label" htmlFor="straatnaam">Straatnaam</label>
                    <input type="text"
                    className="register_form_input"
                    name="straatnaam"
                    placeholder="Typ hier de straatnaam in"
                    value={this.state.straatnaam}
                    onChange={this.onChange}
                    required
                    />
                </section>
                <button className="register_form_submit" type="submit">Verder</button>
             </section>
            </form>
            <aside className="register_aside">
                <Link to="registerbezorger" className="register_aside_bezorger">Registreer als bezorger</Link>
                <Link to="registerklant" className="register_aside_restaurant">Registreer als Klant</Link>
                <hr className="register_aside_line"/>
            </aside>
        </section>
    </section>
    );
}

}

export default RegisterRestaurant;
